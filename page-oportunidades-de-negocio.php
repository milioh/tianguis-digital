<?php get_header(); ?>

		<div class="bg-light hidden-xs">
	        <div class="container">
	            <ol class="breadcrumb custom-breadcrumb m-0">
	                <li><a href="<?php bloginfo("site_url"); ?>/">Sistema de Compras Públicas de la Ciudad de México</a></li>
	                <li><a href="#"> Oportunidades de negocio</a></li>
	            </ol>
	        </div>
	    </div>
	
	    <?php get_template_part("includes/navbar","oportunidades"); ?>
	
	    <div class="main-banner">
	        <div class="container">
	            <h1 class="h1-sm main-banner__title">En este espacio encontrarás proyectos de contratación pública en la Ciudad de México que están en planeación y discusión.</h1>
	            <p class="main-banner__excerpt m-0">Aporta datos, evidencia y recomendaciones para comprar en mejores condiciones y obtener mejores resultados.</p>
	            <a href="#drafts" class="btn btn-primary btn-lg align-self-center" role="button">Ver proyectos</a>
	        </div>
	    </div>
	
	    <div class="container excerpt-sm">
	        <h2 class="title-md text-secondary text-center">¿Cómo funciona esta herramienta?</h2>
	        <ol class="ordered-list">
	            <li>
	                <p class="m-0">Las dependencias del Gobierno de la Ciudad de México y las alcaldías presentan proyectos de contratación, publicando una versión preliminar de las bases del concurso o licitación también llamadas prebases. Su contenido no es definitivo ni vinculatorio.</p>
	            </li>
	            <li>
	                <p class="m-0">Cualquier persona interesada en la contratación accede sin restricciones al proyecto y consulta los documentos que forman parte de las prebases, incluidos sus anexos técnicos. Estos documentos se hacen públicos para identificar y proponer oportunidades de mejora.</p>
	            </li>
	            <li>
	                <p class="m-0">Por un periodo determinado, las prebases están abiertas para recibir observaciones o sugerencias de cualquier persona, de forma abierta y transparente. Todo comentario se registra y es visible para el conocimiento de todos los interesados.</p>
	            </li>
	            <li>
	                <p class="m-0">Al concluir el periodo de discusión se publica un compendio de todos los comentarios y sugerencias recibidas, que será revisado por la dependencia o alcaldía responsable del proyecto de contratación. Es posible que a partir de su valoración se modifique el proyecto de bases antes de su publicación definitiva.</p>
	            </li>
	        </ol>
	        <hr class="separator">
	    </div>
	
	    <div class="home-docs container" id="drafts">
	        <div class="row">
	            <div class="col-xs-12">
	                <h2 class="title-md text-secondary text-center mb-5">Proyectos en discusión</h2>
	                <div class="home-docs-filters row">
	                    <div class="col-sm-6">
	                        <form>
	                            <label for="doc-text-filter" class="custom-label m-0">Filtra por palabras clave:</label>
	                            <input id="doc-text-filter" type="text" class="form-control input-sm custom-input" placeholder="Todas">
	                        </form>
	                    </div>
	                    <div class="col-sm-3">
	                        <label for="home-select2-filter" class="custom-label m-0">Entidad convocante:</label>
	                        <select class="form-control input-sm custom-input" id="home-select2-filter">
	                            <option value="" selected>Todas</option>
	                            <option value="1">Entidad 1</option>
	                            <option value="2">Entidad 2</option>
	                            <option value="3">Entidad 3</option>
	                            <option value="4">Entidad 4</option>
	                            <option value="5">Entidad 5</option>
	                        </select>
	                    </div>
	                    <div class="col-sm-3">
	                        <label for="home-select2-order" class="custom-label m-0">Ordenar por:</label>
	                        <select class="form-control input-sm custom-input" id="home-select2-order">
	                            <option value="fecha">Fecha de publicación</option>
	                            <option value="status">Estatus</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="docs-list row">
	                    <div class="col-12 col-xs-6 col-sm-4 col-md-3">
	                        <div class="card">
	                            <div class="card__header">
	                                <div class="card__label text-center">
	                                    <span class="excerpt-xs">Discusión abierta</span>
	                                </div>
	                                <img src="https://source.unsplash.com/random/300x90" alt="">
	                            </div>
	                            <div class="card__body">
	                                <div class="card__dates excerpt-xxs">
	                                    <ul class="list-unstyled">
	                                        <li><p class="m-0">Publicación</p></li>
	                                        <li><p class="m-0">14-01-2019, 9:00 hrs</p></li>
	                                    </ul>
	                                    <ul class="list-unstyled">
	                                        <li><p class="m-0">Límite para comentar</p></li>
	                                        <li><p class="m-0">18-01-2019, 23:59 hrs</p></li>
	                                    </ul>
	                                </div>
	                                <h3 class="card__title">
	                                    <a href="<?php bloginfo("site_url"); ?>/reader" class="link-absolute">Adquisición de prendas de protección</a>
	                                </h3>
	                                <p class="excerpt-xxs m-0">Entidad convocante</p>
	                                <div class="card__entity">
	                                    <p class="excerpt-xxs m-0">Servicio de Aguas de la Ciudad de México (SACMEX)</p>
	                                </div>
	                                <div class="text-center">
	                                    <a class="btn btn-sm btn-primary pos-r" href="<?php bloginfo("site_url"); ?>/reader">Participa</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-xs-12">
	                        <div class="docs-pagination text-center">
	                            <pagination class="pagination-sm custom-pagination" max-size="10" rotate="false" total-items="totalDocs" items-per-page="perPage" previous-text="Página anterior" next-text="Siguiente página" ng-model="page" ng-change="paginate()"></pagination>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>

<?php get_footer(); ?>