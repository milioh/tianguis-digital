<?php get_header(); ?>

	<div class="document-wrapper">

        <div class="bg-light hidden-xs">
            <div class="container">
                <ol class="breadcrumb custom-breadcrumb m-0">
                    <li><a href="<?php bloginfo("url"); ?>/">Sistema de Compras Públicas de la Ciudad de México</a></li>
                    <li><a href="#">Oportunidades de negocio</a></li>
                </ol>
            </div>
        </div>

        <?php get_template_part("includes/navbar","proyecto"); ?>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="title-section">Datos principales</h2>
                </div>
                <div class="col-md-8">
                    <div class="doc-description doc-wrapper row">
                        <div class="col-xs-12">
                            <dl>
                                <dt>Nombre del proyecto</dt>
                                <dd><?php the_title(); ?></dd>
                            </dl>
                        </div>
                        <div class="col-12 col-xs-6">
                            <dl>
                                <dt>Fecha de publicación</dt>
                                <dd>14-01-2019, 9:00 hrs</dd>
                            </dl>
                        </div>
                        <div class="col-12 col-xs-6">
                            <dl>
                                <dt>Fecha límite para recibir comentarios</dt>
                                <dd>18-01-2019, 23:59 hrs</dd>
                            </dl>
                        </div>
                        <div class="col-xs-12" ng-repeat="date in doc.dates">
                            <dl>
                                <dt>Entidad convocante</dt>
                                <dd>Servicio de Aguas de la Ciudad de México (SACMEX)</dd>
                            </dl>
                        </div>
                        <div class="col-xs-12">
                            <dl>
                                <dt>Unidad responsable</dt>
                                <dd>Dirección General de Recursos Materiales</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h2 class="title-section">Descripción del proyecto</h2>
                    <div class="doc-extract excerpt-sm text-normal">
                        <div class="markdown">SACMEX busca comprar prendas de protección para su plantilla de 1,500 empleados en las áreas de atención de fugas y mantenimiento del sistema de drenaje de la Ciudad de México. Estas prendas deberán cumplor con los estándares de calidad y seguridad de acuerdo a las normas mexicanas vigentes y entregarse en un plazo no mayor a 3 meses.</div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h2 class="title-section">¿Cómo participar en la discusión?</h2>
                    <ol class="ordered-list ordered-list--lg">
                        <li>
                            <p class="m-0">Regístrate e inicia sesión para poder activar las herramientas de participación.</p>
                        </li>
                        <hr>
                        <li>
                            <p class="m-0">Lee y revisa el proyecto de contratación completo.</p>
                        </li>
                        <hr>
                        <li>
                            <p class="m-0">Añade la información que consideres pertinente en el panel de discusión:</p>
                        </li>
                    </ol>
                    <div class="document-help text-normal text-secondary">
                        <div>
                            <p class="excerpt-sm mt-5">Información <span class="document-help__selected">relevante<span></span></span> sobre el proyecto.</p>
                            <p class="text-bold">a) Realiza una anotación sobre una sección del documento.</pv>
                            <p class="excerpt-sm m-0 text-light">Haz una selección con el cursor y da clic en el ícono para añadir un comentario.</p>
                        </div>
                        <div>
                            <img src="<?php bloginfo("template_directory"); ?>/public/images/screenshot.png" alt="Ejemplo de comentario" class="img-responsive document-help__img">
                            <p class="text-bold">b) Publica un comentario sobre el proyecto.</p>
                            <p class="excerpt-sm m-0 text-light">Al final del pánel de discusión escribe un comentario.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-gray-light">
            <div class="container">
                <div class="row">
                    <h2 class="title-section mt-5">Panel de discusión:</h2>
                    <div class="col-md-3 doc-sidebar">
                        <div id="doc-sidebar"></div>
                    </div>
                    <div class="doc-content excerpt-sm text-normal logged_in col-md-9" id="reader-content">
                        <div id="doc_content" class="doc-content-main">
                            <h4>1. Información General</h4>
                            <h5>1.1 Alcance de la Licitación.</h5>
                            <p>La Licitación consiste en la adquisición de los bienes descritos en el Anexo Uno. La contratación abarcará el ejercicio fiscal 2018.</p>
                            <h5>1.2 Disponibilidad Presupuestal.</h5>
                            <p>La presente Licitación cuenta con suficiencia presupuestal autorizada para ejercer los recursos con cargo a la Partida 2721 Prendas de seguridad y protección personal, correspondiente a Recursos Federales-Participaciones a Entidades Federativas y Municipios-Participaciones en Ingresos Federales -Fondo 15-O-1-8-0-.</p>
                            <h5>1.3 Participación de licitantes</h5>
                            <p>En la presente Licitación podrán participar personas físicas o morales de nacionalidad Mexicana y Extranjeros cualquiera que sea el origen de los bienes.</p>
                            <h5>1.4 Padrón de Proveedores de la Administración Pública de la Ciudad de México.</h5>
                            <p>Para participar en procedimientos de contratación, las personas físicas o morales interesadas, deberán estar registrados en el Padrón de Proveedores de la Administración Pública de la Ciudad de México, de acuerdo con los Lineamientos Generales del Padrón de Proveedores de la Administración Pública de la Ciudad de México, publicados el 24 de mayo de 2017, en la Gaceta Oficial de la Ciudad de México. La inscripción es exigible y deberá demostrarse mediante la presentación de la Constancia de Registro o su correspondiente actualización, la cual podrá incluir la calidad de proveedor salarialmente responsable.</p>
                            <h5>1.5 Responsables de la Licitación.</h5>
                            <p>En cumplimiento a lo dispuesto en los Lineamientos para la presentación de Declaración de Intereses y Manifestación de No Conflicto de Intereses a cargo de las personas servidoras públicas de la Administración Pública del Distrito Federal y Homólogos que se señalan, publicados en la Gaceta Oficial del Distrito Federal el 23 de julio de 2015, que señalan; que para cumplir con los Valores y Principios que Rigen el Servicio Público y para Prevenir la Existencia de Conflicto de Intereses, los servidores públicos del SACMEX que cuentan con facultades originarias, por delegación, comisión que actúan en representación como responsables en la toma de decisiones en el presente procedimiento, son: el Ing. Ramón Aguirre Díaz, Director General del SACMEX; el Ing. Leonardo Estrada García, Director General de Administración; el Lic. Miguel Ángel Gutiérrez Acevedo, Director de Recursos Materiales y Servicios Generales y el C. Rafael Roldán Arroyo, Subdirector de Adquisiciones.</p>
                            <h5>1.6 Conflicto de intereses.</h5>
                            <p>Se requiere a todo particular interesado en el presente acto o procedimiento, la presentación por escrito de una manifestación bajo protesta de decir verdad, en el sentido de que no se encuentra en los supuestos de impedimentos legales correspondientes, ni inhabilitados o sancionados por la Contraloría General de la Ciudad de México, por la Secretaría de la Función Pública de la Administración Pública Federal o autoridades competentes de los gobiernos de las entidades federativas o municipios. Asimismo, para prevenir y evitar la configuración de conflicto de intereses, todo particular interesado deberá de manifestar bajo protesta de decir verdad que los socios, directivos, accionistas, administradores, comisarios y demás personal de sus procesos de ventas, comercialización, relaciones públicas o similares, no tienen, no van a tener en el siguiente año o han tenido en el último año relación personal, profesional, laboral, familiar o de negocios con las personas servidoras públicas antes señaladas.</p>
                            <h5>1.7 Responsables de la Evaluación Cuantitativa y Cualitativa.</h5>
                            <h6>1.7.1 Evaluación de la Documentación Legal y Administrativa, Propuesta Económica y Garantía de formalidad de la propuesta. </h6>
                            <p>Los servidores públicos del SACMEX que realizarán la evaluación cuantitativa y cualitativa de los Requisitos de participación: Documentación Legal y Administrativa, Propuesta Económica y Garantía de formalidad de la propuesta, son: el Lic. Miguel Ángel Gutiérrez Acevedo, Director de Recursos Materiales y Servicios Generales y el C. Rafael Roldán Arroyo, Subdirector de Adquisiciones. </p>
                            <h6>1.7.2 Evaluación de la Propuesta Técnica. </h6>
                            <p>Los servidores públicos del SACMEX responsables de realizar la evaluación cuantitativa y cualitativa de los Requisitos de participación: Propuesta Técnica son: el Ing. Alejandro Martínez Pérez, Director Ejecutivo de Operación, el Ing. Miguel Carmona Suárez, Director de Drenaje, Tratamiento y Reuso y el Ing. Darío A. Munguía Torres, Subdirector de Desazolve “A”.</p>
                            <h5>1.8 Contraloría Ciudadana. </h5>
                            <p>El SACMEX invitará a participar en los eventos del procedimiento a un Contralor Ciudadano, designado por la Dirección General de Contralorías Ciudadanas de la Contraloría General de la Ciudad de México.</p>
                            <h5>1.9 No discriminación. </h5>
                            <p>En el presente procedimiento y en el contrato que se celebre, está prohibida cualquier forma de discriminación, sea por acción u omisión, por razones de origen étnico o nacional, género, edad, discapacidad, condición social, condiciones de salud, religión, opiniones, preferencia o identidad sexual o de género, estado civil, apariencia exterior o cualquier otra análoga. </p>
                            <h4>2. Información de los bienes, adjudicación y contrato. </h4>
                            <h5>2.1 Especificaciones, unidades y cantidades. </h5>
                            <p>Los interesados en participar deberán ofertar bienes que cumplan con las especificaciones técnicas, unidad de medida y cantidad contenidas en el Anexo Uno. La Licitación consta de 8 (ocho) partidas de bienes en 3 (tres) hojas de especificaciones. Se deberá ofertar la cantidad total de los bienes de cada una de las partidas.</p>
                            <h6>2.1.1 Bienes de Menor Grado de Impacto Ambiental.</h6>
                            <p>Se deberá ofertar bienes que cumplan con alguno de los supuestos de menor impacto ambiental, que se señalan a continuación:</p>
                            <ol>
                                <li>
                                    <p>Que al final de su vida útil permita su reciclaje</p>
                                </li>
                                <li>
                                    <p>Si existe certificación bajo Normas Oficiales Mexicanas, Normas Mexicanas o Internacionales reconocidas por la Administración Pública de la Ciudad de México, que el producto está certificado. El o los supuestos que se cumplan de los anteriores incisos, los deberá indicar en su Propuesta Técnica y Propuesta Económica el licitante. </p>
                                </li>
                            </ol>
                            <h5>2.2 Modificación de cantidades.</h5>
                            <p>El SACMEX en cualquier etapa del procedimiento, previo al pronunciamiento del Fallo, podrá modificar hasta un 25% (veinticinco por ciento) la cantidad de bienes a contratar, siempre y cuando, existan razones debidamente fundadas o causas de interés público, caso fortuito o fuerza mayor. </p>
                            <h5>2.3 Calidad.</h5>
                            <p>Los bienes deberán ser nuevos, cumplir con las especificaciones contenidas en el Anexo Uno, con las Normas Oficiales Mexicanas correspondientes, Normas Mexicanas y a falta de éstas, con las Normas Internacionales o, en su caso, las Normas de referencia aplicables o las de calidad del fabricante, de conformidad con lo dispuesto por los artículos 53, 55 y 67 de la Ley Federal sobre Metrología y Normalización.</p>
                            <h5>2.4 Contenido Nacional.</h5>
                            <p>Los bienes que se oferten y entreguen no requieren cumplir con un porcentaje mínimo de contenido de integración nacional, de acuerdo con las disposiciones emitidas por la Secretaría de Desarrollo Económico de la Ciudad de México.</p>
                            <h5>2.5 Plazo de entrega.</h5>
                            <p>Los bienes deberán entregarse y ser aceptados por el SACMEX, máximo el 27 de diciembre de 2018. Si el vencimiento fuera inhábil se recorrerá al día hábil siguiente. Los bienes se aceptarán en entregas parciales por partidas completas, mediante remisión o factura e invariablemente deberán acompañarse del Acta de Aceptación de bienes que emitirá la Unidad Departamental de Almacén Central con la aprobación de la Dirección Ejecutiva de Operación del SACMEX.</p>
                            <h5>2.6 Lugar y horario de entrega.</h5>
                            <h6>2.6.1 Almacén Central del SACMEX.</h6>
                            <p>Los bienes deberán entregarse L.A.B. destino, en el Almacén Central del SACMEX, sito en Sur 24 No. 351, Colonia Agrícola Oriental, Alcaldía Iztacalco, Código Postal 08500, en la Ciudad de México, de lunes a viernes de 8:00 a 18:00 horas, a nivel de piso en bodega.</p>
                            <h6>2.6.2 Condiciones de entrega de los bienes.</h6>
                            <p>El Proveedor deberá informar previamente al SACMEX la fecha y horario en que realizará la entrega, mediante escrito dirigido a Dirección Ejecutiva de Operación, con copia a la Dirección de Recursos Materiales y Servicios Generales, a la Subdirección de Almacenes y a la Unidad Departamental del Almacén Central, con la finalidad de contar con la presencia de sus representantes para verificar la recepción y, en su caso, determinar la aceptación o rechazo de los bienes, así como la liberación de la factura de pago. El servidor público representante de la Dirección Ejecutiva de Operación del SACMEX, responsable para verificar la recepción y, en su caso, determinar la aceptación o rechazo de los bienes, así como la liberación de la factura de pago y el seguimiento al fiel cumplimento de todas y cada una de las condiciones contractuales es el Ing. Darío A. Munguía Torres, Subdirector de Desazolve “A”. El SACMEX en ningún caso recibirá bienes que no cumplan con las especificaciones requeridas en el Anexo Uno y en la o las Juntas de Aclaración de Bases, así como las establecidas en el contrato respectivo.</p>
                            <h5>2.7 Embalaje, empaque, fletes y maniobras.</h5>
                            <p>Los bienes deberán suministrarse con embalaje y empaque para su traslado y almacenaje en buen estado de acuerdo a sus características técnicas, quedando a cargo del Proveedor el costo de fletes y maniobras de descarga en el lugar de entrega. El embalaje y empaque deberá ser resistente para soportar la manipulación ordinaria en maniobras de carga y descarga a la que estarán sujetos los bienes.</p>
                            <h5>2.8 Garantía de los bienes.</h5>
                            <p>Los bienes deberán contar con garantía por un periodo mínimo de 12 meses contados a partir de la puesta en operación o de 18 meses contados a partir de la recepción de los bienes. En caso que los bienes presenten fallas durante el periodo de garantía, se deberá proceder al reemplazo o reparación de los bienes; lo que deberá realizarse de conformidad al plazo que determine la Dirección Ejecutiva de Operación del SACMEX, quien notificará por escrito al Proveedor con copia a la Dirección de Recursos Materiales y Servicios Generales del SACMEX. El plazo para el remplazo o reparación de los bienes no podrá ser mayor al 50% (cincuenta por ciento) del plazo establecido originalmente en el contrato para la entrega de los bienes. En caso de que el Proveedor no realice el reemplazo o reparación de los bienes, en el plazo establecido en el párrafo precedente, el SACMEX procederá a la aplicación de la garantía de cumplimiento del contrato.</p>
                            <h5>2.9 Defectos, vicios ocultos y deficiencia en la calidad.</h5>
                            <p>El Proveedor quedará obligado a responder de defectos, vicios ocultos y deficiencia en la calidad de los bienes, así como cualquier otra responsabilidad en que incurra en los términos del contrato que celebre y en el Código Civil para el Distrito Federal.</p>
                            <h5>2.10 Patentes, marcas y derechos de autor.</h5>
                            <p>El licitante o Proveedor, según sea el caso, asumirá la responsabilidad total que resulte de cualquier violación a las disposiciones legales inherentes a la Propiedad Industrial o Derechos de Autor, que surjan con motivo de este Procedimiento o del suministro de los bienes contratados por el SACMEX.</p>
                            <h5>2.11 Información adicional.</h5>
                            <p>Los bienes deberán contar con información técnica: catálogos y/o folletos, en original o copia legible, en español o inglés (acompañada de traducción al español de las características más relevantes), preferentemente en el primero, que contengan las generalidades y características técnico-operativas, éstas corresponderán a las características que se describan en la Propuesta Técnica de acuerdo con lo solicitado en el Anexo Uno.</p>
                        </div>
                    </div>
                </div>

                <div class="mt-5">
                    <h3>Comentarios:</h3>
                    <div id="participate-comment" class="participate-comment">
                        <div class="comment-field">
                            <form name="add-comment-form">
                                <div class="form-group">
                                    <textarea class="form-control centered input-comment" name="doc-comment-field" id="doc-comment-field" required>Escribe tu comentario</textarea>
                                </div>
                                <div class="text-right mb-4">
                                    <button type="reset" class="btn btn-primary btn-x-md hidden-xs">Cancelar</button>
                                    <button type="submit" class="btn btn-primary btn-x-md ml-2">Publicar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="activity-thread">
                        <div class="activity-item my-6">
                            <dl>
                                <dt>Fecha:</dt>
                                <dd>01/05/2019</dd>
                            </dl>
                            <dl>
                                <dt>Autor:</dt>
                                <dd>Jon Doe</dd>
                            </dl>
                            <dl>
                                <dt>Comentario:</dt>
                                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas ducimus vitae nemo enim nulla ratione necessitatibus harum fugiat eaque officiis nostrum deserunt accusantium dolor, quia in doloribus voluptatibus adipisci ipsa.</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="activity-thread">
                        <div class="activity-item my-6">
                            <dl>
                                <dt>Fecha:</dt>
                                <dd>01/05/2019</dd>
                            </dl>
                            <dl>
                                <dt>Autor:</dt>
                                <dd>Jon Doe</dd>
                            </dl>
                            <dl>
                                <dt>Comentario:</dt>
                                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia tenetur quis et, possimus doloribus quisquam ea rerum voluptatem minima molestiae sunt sequi aperiam quod eos adipisci magnam. Perferendis, porro id. Qui quidem exercitationem similique cumque aut hic amet voluptatibus velit placeat possimus at eius, error, temporibus eaque maxime omnis, doloribus culpa provident atque et distinctio nam ab asperiores. Libero saepe qui quaerat quidem quisquam consequatur, repellendus unde sed voluptatibus quam aliquam necessitatibus suscipit corporis totam. Voluptas minima ex ipsum nam deleniti culpa deserunt expedita, vitae qui esse, laudantium sunt mollitia dolorum accusantium animi error quia repudiandae molestias. Earum, delectus consectetur.</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>