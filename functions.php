<?php

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Tianguis Digital</i>';
	}
	function my_footer_version() {
	    return 'Version 1.0';
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load CSS
		wp_enqueue_style( 'styles', THEMEPATH . 'style.css', array(), '1.0.0' );
		// Load JS
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'app.js', array(), '1.0.0', false );
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );
	
	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {

		//Proyectos
		$labels = array(
		    'name' => _x('Proyectos', 'post type general name'),
		    'singular_name' => _x('Proyecto', 'post type singular name'),
		    'add_new' => _x('Agregar Proyecto', 'New'),
		    'add_new_item' => __('Agregar Nuevo Proyecto'),
		    'edit_item' => __('Editar Proyecto'),
		    'new_item' => __('Nuevo Proyecto'),
		    'all_items' => __('Todos los Proyectos'),
		    'view_item' => __('Ver Proyecto'),
		    'search_items' => __('Buscar Proyectos'),
		    'not_found' =>  __('Proyecto no encontrado'),
		    'not_found_in_trash' => __('Proyecto no encontrado en papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Proyectos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-admin-multisite',
		    'supports' => array( 'title' )
		);
		register_post_type('proyecto',$args);
		
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}
	

?>