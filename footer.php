	    
	    <footer class="footer mt-6">
	        <div class="container footer__row-top text-center">
	            <p class="mb-1 excerpt-sm text-dark excerpt-xs-mobile">Los proyectos de contratación aquí presentados son preliminares. Su contenido no es definitivo ni vinculatorio.</p>
	            <p class="excerpt-sm text-dark excerpt-xs-mobile"><strong>Éste se hace de conocimiento público con el fin de ser discutido de forma abierta y transparente.</strong></p>
	        </div>
	        <div class="bg-dark footer__row-bottom">
	            <div class="container navigation">
	                <img src="<?php bloginfo("template_directory"); ?>/public/images/logo-adip.svg" alt="Agencia Digital de Innovación Publica" class="footer__logo">
	                <div>
	                    <p class="m-0">Tianguis Digital: Sistema de Compras Públicas de la Ciudad de México</p>
	                    <p class="m-0"><strong>Diseñado y operado por la Agencia Digital de Innovación Pública</strong></p>
	                </div>
	                <ul class="list-inline m-0 navigation__nav hidden-xs">
	                    <li><a href="<?php bloginfo("template_directory"); ?>/normatividad" class="link-static">Normatividad</a></li>
	                    <li><a href="<?php bloginfo("template_directory"); ?>/contacto" class="link-static">Contacto</a></li>
	                </ul>
	            </div>
	        </div>
	    </footer>
	    <input type="hidden" id="base_url" name="base_url" value="<?php bloginfo("template_directory"); ?>" />
	    <?php wp_footer(); ?>
	</body>
</html>
