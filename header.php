<?php
    global $post;
    $post_slug = $post->post_name;
    if (!$post_slug) { $post_slug = 'home'; }
    if (is_single()) { $post_slug = $post->post_type; } 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
	<body class="<?php echo $post_slug; ?>">
		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->
	    <header class="navbar-fixed-top navbar navbar-light bg-white navbar-custom">
	        <div class="container">
	            <h1 class="m-0 navbar-brand">
	                <a href="<?php bloginfo("url"); ?>">
	                    <img src="<?php bloginfo("template_directory"); ?>/public/images/logo-cdmx.svg" alt="Tianguis Digital: Sistema de Compras Públicas de la Ciudad de México">
	                </a>
	            </h1>
	            <div class="navbar-brand-secondary">
	                <h2 class="text-uppercase">
	                    <a href="<?php bloginfo("site_url"); ?>" target="_self" class="link-static">Tianguis Digital (beta)</a>
	                </h2>
	            </div>
	        </div>
	    </header>