<?php get_header(); ?>

    	<div class="bg-light hidden-xs">
	        <div class="container">
	            <ol class="breadcrumb custom-breadcrumb m-0">
	                <li><a href="<?php bloginfo("url"); ?>">Sistema de Compras Públicas de la Ciudad de México</a></li>
	                <li><a href="<?php bloginfo("url"); ?>/reader"> Oportunidades de negocio</a></li>
	            </ol>
	        </div>
	    </div>
	    
	    <?php get_template_part("includes/navbar","oportunidades"); ?>

		<div class="home-docs container">
        	<div class="row">
            <div class="col-xs-12">
                <h2 class="title-md text-secondary m-0 mt-5">Histórico</h2>
                <h3 class="text-normal mt-0 mb-4">Proyectos de contratación discutidos</h3>
                <div class="home-docs-filters row">
                    <div class="col-sm-6">
                        <form>
                            <label for="doc-text-filter" class="custom-label m-0">Filtra por palabras clave:</label>
                            <input id="doc-text-filter" type="text" class="form-control input-sm custom-input" placeholder="Todas">
                        </form>
                    </div>
                    <div class="col-sm-3">
                        <label for="home-select2-filter" class="custom-label m-0">Entidad convocante:</label>
                        <select class="form-control input-sm custom-input" id="home-select2-filter">
                            <option value="" selected>Todas</option>
                            <option value="1">Entidad 1</option>
                            <option value="2">Entidad 2</option>
                            <option value="3">Entidad 3</option>
                            <option value="4">Entidad 4</option>
                            <option value="5">Entidad 5</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label for="home-select2-order" class="custom-label m-0">Ordenar por:</label>
                        <select class="form-control input-sm custom-input" id="home-select2-order">
                            <option value="fecha">Fecha de publicación</option>
                            <option value="status">Estatus</option>
                        </select>
                    </div>
                </div>
                <?php
	                //Query Proyectos
	                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	                $args = array(
						'post_type' 	   => 'proyecto',
						'posts_per_page'   => 12,
						'order_by'	 	   => 'date',
						'order' 		   => 'DESC',
						'post_status'      => 'publish',
						'paged'			   => $paged,
						'suppress_filters' => false
					);
					$query_proyectos = new WP_Query( $args );
	            ?>
                <div class="docs-list row">
	                <?php foreach ($query_proyectos->posts as $post) { setup_postdata( $post ); ?>
                    <div class="col-12 col-xs-6 col-sm-4 col-md-3">
                        <div class="card">
                            <div class="card__header">
                                <div class="card__label text-center">
                                    <span class="excerpt-xs">Discusión finalizada</span>
                                </div>
                                <img src="https://source.unsplash.com/random/300x90" alt="">
                            </div>
                            <div class="card__body">
                                <div class="card__dates excerpt-xxs">
                                    <ul class="list-unstyled">
                                        <li><p class="m-0">Publicación</p></li>
                                        <li><p class="m-0">14-01-2019, 9:00 hrs</p></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><p class="m-0">Límite para comentar</p></li>
                                        <li><p class="m-0">18-01-2019, 23:59 hrs</p></li>
                                    </ul>
                                </div>
                                <h3 class="card__title">
                                    <a href="<?php the_permalink(); ?>" class="link-absolute"><?php the_title(); ?></a>
                                </h3>
                                <p class="excerpt-xxs m-0">Entidad convocante</p>
                                <div class="card__entity">
                                    <p class="excerpt-xxs m-0">Servicio de Aguas de la Ciudad de México (SACMEX)</p>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-sm btn-default pos-r" href="<?php bloginfo("url"); ?>/reader">Revisar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); } ?>
                    <div class="col-xs-12">
                        <div class="docs-pagination text-center">
                            <pagination class="pagination-sm custom-pagination" max-size="10" rotate="false" total-items="totalDocs" items-per-page="perPage" previous-text="Página anterior" next-text="Siguiente página" ng-model="page" ng-change="paginate()"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		</div>
    
<?php get_footer(); ?>