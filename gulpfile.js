var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCss = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    babel = require('babelify'),
    buffer = require('vinyl-buffer'),
    watchify = require('watchify');

var SCRIPTS_DIR = 'resources/js';
var STYLES_DIR = 'resources/sass';

gulp.task('css', function() {
	return gulp
    .src(STYLES_DIR+'/app.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        grid: true
    }))
    .pipe(cleanCss())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('.'));
});

function compile(watch) {
    var bundler = watchify(browserify(SCRIPTS_DIR+'/app.js', {
        debug: true,
        presets: ["es2015"]
    }).transform(babel));

    function rebundle() {
      bundler.bundle()
        .on('error', function(err) { console.error(err); this.emit('end'); })
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./js'));
    }

    if (watch) {
      bundler.on('update', function() {
        console.log('-> bundling...');
        rebundle();
      });
    }

    rebundle();
  }

  function watch() {
    return compile(true);
  };

gulp.task('build', function() { return compile(); });

gulp.task('watch', function () {
  gulp.watch(STYLES_DIR+'/**/*.scss', ['css']);
  return watch();
});

gulp.task('default', ['watch']);
