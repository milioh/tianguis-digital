document.onreadystatechange = function () {
    if (this.readyState === "complete") {
        var contentBlock = document.getElementById("doc_content");
        if (!contentBlock) {
            return;
        }
        for (var level = 1; level <= 6; level++) {
            linkifyAnchors(level, contentBlock);
        }
    }
};

let linkifyAnchors = function (level, containingElement) {
    let headers = containingElement.getElementsByTagName("h" + level),
        container = document.getElementById("doc-sidebar");
    for (let h = 0; h < headers.length; h++) {
        let header = headers[h];
        header.id = header.innerHTML.toLowerCase().replace(" ", "-");

        container.appendChild(anchorForId(header.id));
    }
};

let anchorForId = function (id) {
    let anchor = document.createElement("a");
    anchor.className = "doc-sidebar__item";
    anchor.href      = "#" + id;
    anchor.innerHTML = id;
    return anchor;
};
