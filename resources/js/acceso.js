$( document ).ready(function() {
    
    //Read Values
	var base_url = $('#base_url').val();
    
    //btnLoginAdmin click
    $('#btnLoginAdmin').on('click', function(e) {
	    e.preventDefault();
	    
	    //Read Values
	    var email = $('#accesoEmail').val();
		var password = $('#accesoPassword').val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		//Check Valid Form
		if (regex.test(email) && $.trim(password))
		{
			//Read Values
			var param = '{"msg": "loginAdmin","fields": {"email": "' + email + '", "password": "' + password + '"}}';

			//Disable Button
			$('#btnLoginAdmin').prop( 'disabled', true );
			$('#btnLoginAdmin').html('Procesando...');

			//API Call
			$.post(base_url + 'api', { param: param }).done(function( data ) {
				console.log(data);
				//Check Status Call
				if (data.status == 1)
				{
					//Reload
					location.reload();
				}
				else
				{
					//Enable Button
					$('#btnLoginAdmin').prop('disabled', false);
	                $('#btnLoginAdmin').html('Iniciar Sesión');
				}
			});
		}
		else
		{
			console.log('no access');
		}
	    
	    return false;
    });
});