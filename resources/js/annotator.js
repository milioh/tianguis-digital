const annotator = require('annotator');

let target = document.getElementById("reader-content");

if (target) {

    let pageUri = function () {
        return {
            beforeAnnotationCreated: function (ann) {
                ann.uri = window.location.href;
            }
        };
    };

    const app = new annotator.App()
        .include(annotator.ui.main, { element: target })
        .include(annotator.storage.http, { prefix: '/annotation' })
        .include(pageUri);

    app.start()
    .then(function () {
        app.annotations.load({ uri: window.location.href });
    });
}
